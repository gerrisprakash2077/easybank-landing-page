let num = 1;
function navigation() {
    let nav = document.getElementById("mobile-navigation-box");
    let background = document.getElementById("mobile-headder-button-background");
    nav.style.color = "hsl(233, 26%, 24%)";
    nav.style.fontSize = "17px";
    nav.style.position = "fixed";
    nav.style.zIndex = "2";
    nav.style.backgroundColor = "white";
    nav.style.textAlign = "center";
    nav.style.paddingRight = "130px";
    nav.style.paddingLeft = "130px";
    nav.style.paddingTop = "15px";
    nav.style.paddingBottom = "15px";
    nav.style.margin = "30px";
    nav.style.borderRadius = "5px";
    nav.style.lineHeight = "50px";
    nav.style.boxShadow = "rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px";
    nav.innerHTML = `
        <nav>
            <p id="js-item"><a>Home</a></p>
            <p><a>About</a></p>
            <p><a>Contact</a></p>
            <p><a>Blog</a></p>
            <p><a>Careers</a></p>
        </nav>`
    if (num % 2 == 1) {
        background.src = './images/icon-close.svg';
        num++;
        nav.style.visibility = "visible";

    } else {
        background.src = './images/icon-hamburger.svg';
        num++;
        nav.style.visibility = "hidden";
    }
}